---
layout: post
title:  "Gitlab Pages - Jekyll sur Framagit"
date:   2017-11-02 22:06:12 +0100
categories: framasoft libre
---

# (WIP) Gitlab Pages avec Jekyll sur Framagit

Depuis quelques temps, j'essaie de trouver une alternative à **Ghost** car les dernières versions *forcent* à utiliser une base de données type MySQL à la place de Sqllite. Je peux comprendre la nécessité d'une *vraie* base de données. Mais dans mon cas, ça me parait un peu usine à gaz.

J'ai donc commencer à chercher une alternative qui correspondrait à mon besoin. Voici les solutions que l'on m'a proposé :

* Pluxml
* Pélican
* 

![Failure]({% asset_path  pingouinVolantRefait.png %})
